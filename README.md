## State Log

Simple module, which logs any changes to State. Can also configure which
State keys get ignored.

### Requirements

No requirements, besides State, which is provided via Drupal core.

### Install/Usage

* Install/enable like any other contributed module.

* Configure any State keys that you wish to ignore or to not log.

* Review "Recent log messages" page for State log entries:
**/admin/reports/dblog**


### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
