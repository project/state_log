<?php

namespace Drupal\state_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure State Log settings.
 */
class StateLogConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'state_log_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['state_log.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('state_log.settings');

    $description = $this->t('One state name per line.<br />Examples:
      <ul>
        <li>system.theme.files</li>
        <li>drupal_css_cache_files</li>
        <li>system.maintenance_mode</li>
      </ul>', [
        '%url' => 'https://www.drupal.org/docs/8/api/state-api/overview'
    ]);

    $form['options']['log_ignored_state'] = [
      '#type' => 'textarea',
      '#rows' => 15,
      '#title' => $this->t('State names to ignore'),
      '#description' => $description,
      '#default_value' => !empty($config->get('log_ignored_state')) ? implode(PHP_EOL, $config->get('log_ignored_state')) : '',
      '#size' => 60,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ignore_settings_array = preg_split("[\n|\r]", $form_state->getValue('log_ignored_state'));
    $ignore_settings_array = array_filter($ignore_settings_array);

    $this->config('state_log.settings')
      ->set('log_ignored_state', $ignore_settings_array)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
