<?php

namespace Drupal\state_log;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\State\StateInterface;

/**
 * Logs changes to state.
 */
class StateLog implements StateInterface {

  use LoggerChannelTrait;

  /**
   * Decorated state service.
   *
   * @var \Drupal\Core\State\StateInterface $state
   */
  protected $state;

  /**
   * The Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new StateLog.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The decorated state service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(StateInterface $state, ConfigFactory $config_factory) {
    $this->state = $state;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function get($key, $default = NULL) {
    return $this->state->get($key, $default);
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys) {
    return $this->state->getMultiple($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $this->log($key, $value);
    $this->state->set($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $data) {
    foreach ($data as $key => $value) {
      $this->log($key, $value);
    }
    $this->state->setMultiple($data);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($key) {
    $this->log($key, NULL);
    $this->state->delete($key);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    foreach ($keys as $key) {
      $this->log($key, NULL);
    }
    $this->state->deleteMultiple($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache() {
    $this->state->resetCache();
  }

  /**
   * Returns whether the state_name is ignored.
   *
   * @param string $state_name
   *   The state name key.
   *
   * @return bool
   *   TRUE if the state key is ignored, FALSE otherwise.
   */
  private function isKeyIgnored(string $state_name): bool {
    $ignored_entities = $this->configFactory->get('state_log.settings')
      ->get('log_ignored_state') ?: [];
    foreach ($ignored_entities as $ignore) {
      if ($ignore == $state_name) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Logs the change to the state data.
   *
   * @param string $key
   *   The key of the data being logged.
   * @param mixed $value
   *   The data being logged.
   */
  protected function log(string $key, $value) {
    // Check and ignore configured keys.
    if (!$this->isKeyIgnored($key)) {
      // Only log items that have changed.
      if ($this->get($key) != $value) {
        // Log state changes.
        $this->getLogger('state_log')
          ->info('State changed: %key changed from %original to %value', [
            '%key' => $key,
            '%original' => var_export($this->get($key), TRUE),
            '%value' => var_export($value, TRUE),
          ]);
      }
    }
  }

}
